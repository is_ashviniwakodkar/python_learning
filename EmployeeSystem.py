class EmployeeSystem:
   listEmployee=[]
   listOrganization =[]
   count=0
   def __init__(self):
     print("\n*******Welcome to emplyees tracking system********")

   def addNewEmployee(self):
       value =0
       try:
           totalCount = int(input("\nCount of employees to add : "))
       except IndexError:
           print("list index out of range")
       except ValueError:
           print("invalid literal for int() with base 10")
       else:
           while value<totalCount:
              a = input("\nEnter employee name, Gender, Job_title, Birthday : ").split(",")
              EmployeeSystem.listEmployee.append(a)
              b = input("\nOrganization_name : ")
              EmployeeSystem.listOrganization.append(b)
              value=value+1
              EmployeeSystem.count=EmployeeSystem.count+1

   def displayCount(self):
        print("\nThe total number of employees in organization : ",self.count)

   def displayEmployeeDetails(self):
       for i in range(len(EmployeeSystem.listEmployee)):
          print("\nDetails of employee are : ",EmployeeSystem.listEmployee[i])
       for j in range(len(EmployeeSystem.listOrganization)):
          print("\nOrganization_name of Employee is : ", EmployeeSystem.listOrganization[j])

   def compareEmployee(self):
        if EmployeeSystem.listEmployee[0] == EmployeeSystem.listEmployee[1]:
            print("\nEmpolyees are same")
        else:
            print("\nEmployees are not same")
    
   def compareOrganization(self):
        if EmployeeSystem.listOrganization[0] == EmployeeSystem.listOrganization[1]:
            print("\nOrganizations are same")
        else:
            print("\nOrganizations are not same")

class Google(EmployeeSystem):
    def __init__(self):
      super().__init__()

class Microsoft(Google):
    def __init__(self):
      super().__init__()

org1 = Microsoft()
org1.addNewEmployee()
org1.displayEmployeeDetails()
org1.displayCount()
org1.compareEmployee()
org1.compareOrganization()

