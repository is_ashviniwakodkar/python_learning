totalCount = int(input("How many numbers : "))
n1=0
n2=1
count = 0

if totalCount <= 0:
   print("Enter a positive integer")
elif totalCount == 1:
   print("Fibonacci series upto",totalCount,":")
   print(n1)
else:
   print("Fibonacci series:")
   while count < totalCount:
       print(n1)
       new = n1 + n2
       n1 = n2
       n2 = new
       count += 1