int_value = 2214 
float_value = 2.2214

new_datatype = int_value + float_value

print("datatype of int_value:",type(int_value))
print("datatype of float_value:",type(float_value))

print("Value of new_datatype:",new_datatype)
print("datatype of new_datatype:",type(new_datatype))