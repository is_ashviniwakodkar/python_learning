int_value = 2214
str_value = "214"

print("Data type of num_int:",type(int_value))
print("Data type of num_str:",type(str_value))

str_value = int(str_value)
print("Data type of num_str after Type Casting:",type(str_value))

sum = int_value + str_value

print("Sum of int and str:",sum)
print("Data type of the sum:",type(sum))