input='GameOfThrones'

# a) extract from input => "one"
newInput = input[9:12]
print('Extracted value : ',newInput)

# b) convert (a) from "one" to => "One"
newOnestring = newInput.capitalize()
print('one after converting to One : ',newOnestring)

# c) reverse the input => "senorhTfOemaG"
reversedInput ='GameOfThrones'[::-1]
print('Input after reversing : ',reversedInput)

# d) reverse the input case => "gAMEoFtHRONES"
newReversedInput= input.swapcase()
print('Reversing input case: ',newReversedInput)

# e) extract caps into one string => "GOT"
upperCaseCharacters = [char for char in input if char.isupper()]
print('The uppercase characters in string are :',upperCaseCharacters )