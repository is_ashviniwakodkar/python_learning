import pytest
import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

driver = None
def setup_module(module):
    global driver
    driver=webdriver.Chrome(ChromeDriverManager().install())
    driver.implicitly_wait(10)
    driver.get("https://pagely.com/")
    print("Title of the page :",driver.title)
   
def teardown_module(module):
    driver.quit()

def test_signUp_navigation():
    driver.find_element(By.XPATH, "//*[@id='navbarContent']/ul[2]/li[2]/a").click()
    elem = driver.find_element(By.XPATH, "//*[@id='signup_content']/div[2]/div[2]/div/div/div[1]/h1")
    assert elem.text =="CHOOSE A PLAN", "header is not correct"
   
def test_invalid_signIn():
    driver.find_element(By.XPATH, "//*[@id='navbarContent']/ul[2]/li[1]/a").click()
    elem = driver.find_element(By.XPATH, "//*[@id='app']/div/div/div[2]")
    assert elem.text=="WELCOME TO ATOMIC!", "Navigation failed to login page"
    username = driver.find_element(By.XPATH,"//*[@id='username']")
    username.send_keys("abcd")
    password = driver.find_element(By.XPATH,"//*[@id='password']")
    password.send_keys("abcd1")
    driver.find_element(By.XPATH, "//div/div[2]/button/span[1]").click()
    elem = driver.find_element(By.XPATH, "//div/div[2]/div[4]/div")
    assert elem.text, "incorrect credentials"
   
def test_dropDown():
    driver.find_element(By.XPATH, "//*[@id='menu-item-9576']/a").click()
    driver.find_element(By.XPATH, "//*[@id='menu-item-9576']/div/a[text()='Enterprise']").click()
    driver.find_element(By.XPATH, "//*[@id='pagely_hero']/div[7]/div/div/div/p/a").click()
    elem = driver.find_element(By.XPATH,"//*[@id='qform']/div/div[1]/div/h2")
    assert elem.text == "REQUEST A PROPOSAL", "navigation failed"

def test_contactNavigation():
    driver.find_element(By.XPATH, "//li/a[text()='Contact']").click()
    driver.find_element(By.XPATH, "//*[@id='field_13_9']/div/div").click()
    driver.find_element(By.XPATH, "//li[2]/span[text()='General Sales']").click()
    name = driver.find_element(By.XPATH,"//*[@id='input_13_7']")
    name.send_keys("abcd")
    email = driver.find_element(By.XPATH,"//*[@id='input_13_3']")
    email.send_keys("abcd1@gmail.com")
    msg = driver.find_element(By.XPATH,"//*[@id='input_13_4']")
    msg.send_keys("first user")
    driver.find_element(By.XPATH, "//input[@id='gform_submit_button_13']").click()
    captcha = driver.find_element(By.XPATH,"//*[@id='validation_message_13_15']")
    assert captcha.text == "The reCAPTCHA was invalid. Go back and try it again.", "validation failed"

def test_linkValidation():
    firstLink = driver.find_element(By.XPATH,"//a[text()='Hosting Solutions']")
    assert firstLink.text == "HOSTING SOLUTIONS", "link is not correct"
    secondLink = driver.find_element(By.XPATH,"//*[@id='menu-item-20995']/a")
    assert secondLink.text == "LEARN", "link is not correct"
    thirdLink = driver.find_element(By.XPATH,"//*[@id='menu-item-9577']/a")
    assert thirdLink.text == "PRICING", "link is not correct"
    fourthLink = driver.find_element(By.XPATH,"//*[@id='menu-item-34']/a")
    assert fourthLink.text == "SUPPORT", "link is not correct"
    fifthLink = driver.find_element(By.XPATH, "//li/a[text()='Contact']")
    assert fifthLink.text == "CONTACT", "link is not correct"
    sixthLink = driver.find_element(By.XPATH, "//*[@id='navbarContent']/ul[2]/li[1]/a")
    assert sixthLink.text == "SIGN IN", "link is not correct"